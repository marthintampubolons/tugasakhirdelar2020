<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <title>Bootstrap 4 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" href="" type="image/x-icon">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"> -->

    <!-- Link to your CSS file -->
    <link rel="stylesheet" href="">
</head>

<body>

    <!-- Start coding here -->
 
        <div class="col-md-9">
            <table class="table table-bordered" bordercolor="black" width="1200px" >
       <thead>
           <tr>
               <th scope="col">#</th>
               <th scope="col">Nama Gedung</th>
               <th scope="col">Information</th>
               <th scope="col">Nomor Gedung</th>
               <th scope="col">Action</th>
           </tr>
       </thead>
       <tbody>
          <?php
           include("includes/db.php");
                   $ref = $_POST['cars'];
           $data = $database->getReference($ref)->getValue();
           $i = 0;
           foreach($data as $key => $data1){
               $i++;
           ?>
           <tr>
               <th scope="row"><?php echo $i; ?></th>
               <td><?php echo $data1['nama_gedung']; ?></td>
               <td><?php echo $data1['informasi']; ?></td>
               <td><?php echo $data1['nomor_gedung']; ?></td>
               <td>
               <a type="button" class="btn btn-danger" href="fungsi_delete.php?key=<?php echo $key; ?>&nama=<?php echo $data1['nama_gedung']; ?>">Delete</a>
               <!-- <a type="button" class="btn btn-success" href="detail.php?key=<?php echo $key; ?>">Success</a> -->
                <button type="button" class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#modal">Update Data</button>
               <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Update Data</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form action="fungsi_update_app.php" method="post" enctype="multipart/form-data">
       <div class="form-group">
           <label for="exampleFormControlInput1">Name Gedung</label>
           <input type="text" class="form-control" name="nama_gedung" value="<?php echo $data1['nama_gedung']; ?>">
       </div>
       <div class="form-group">
           <label for="exampleInputEmail1">Nomor Gedung</label>
           <input type="text" class="form-control" name="nomor_gedung" aria-describedby="emailHelp" value="<?php echo $data1['nomor_gedung']; ?>">
       </div>
       <div class="form-group">
           <label for="exampleFormControlTextarea1">Informasi</label>
           <textarea class="form-control" name="informasi" rows="3" ><?php echo $data1['informasi']; ?></textarea>
       </div>
       <input type="hidden" name="ref" value="<?php echo $data1['nama_gedung']; ?>/<?php echo $key; ?>">
       <button type="submit" name="update" class="btn btn-primary">Submit</button>
   </form>
            </div>
         </div>
      </div>
   </div>
              
               </td>
           </tr>
           <?php 
           }
           ?>
       </tbody>
   </table>

        </div>
    </div>
    <?php
    include("includes/db.php");
    ?>
    <center><h2>
        <?php
        echo $database->getReference($_POST['cars'])->getSnapshot()->numChildren();
        ?>
    </h2></center>
    

    <!-- Coding End -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>